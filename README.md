# Webserver test

Just an unpretentious Docker image to serve as demo app.
Originally built to make some CI/CD and GitOps tests, deploying the same app on multiple environments.

```bash
# Command to use it
docker run -d -p 8080:8080 tperelle/webserver-test:latest 

# Specifying the current environment ENV=[dev|qa|prd]
docker run -d -p 8080:8080 -e ENV=qa tperelle/webserver-test:latest 
```

The page prints current environment name to make check easier.

![screenshot](docs/img/webserver-test.png)