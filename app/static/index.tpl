<!DOCTYPE HTML>
<html lang=en>
<head>
    <meta charset="utf-8">
    <title>Web server test</title>
    <link rel="stylesheet" href="./styles.css">
</head>
<body>
    <h1>Web server test</h1>
    <p>Welcome to <b class="{{ .style }}">{{ .environment }}</b> !</p>
    <img src="./img/docker1.png" alt="docker1"/>
    <footer>Made in Go with ❤️</footer>
</body>
</html>