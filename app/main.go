package main

import (
	"net/http"
	"os"
	"strings"

	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
)

func main() {

	// Default path for static resources
	path := "static/"
	environment := "DEV"

	if len(os.Args[1:]) > 0 && os.Args[1] != "" {
		path = os.Args[1]
		println("Argument found, new path for static resources is: " + path)
	} else {
		println("No argument found, default path is used for static resources: " + path)
	}

	// Check if index.tpl is accessible
	indexPath := path + "index.tpl"
	if _, err := os.Stat(indexPath); err == nil {
		println("Index template found at: " + indexPath)
	} else {
		println("Index template not found at: " + indexPath)
		os.Exit(1)
	}

	// Check the presence of ENV environment variable
	if os.Getenv("ENV") != "" {
		environment = strings.ToUpper(os.Getenv("ENV"))
		println("Environment name provided: " + environment)
	} else {
		println("Default environment name used: " + environment)
	}

	router := gin.Default()
	router.LoadHTMLFiles(indexPath)
	router.Use(static.Serve("/", static.LocalFile(path, false)))
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tpl", gin.H{
			"environment": environment,
			"style":       strings.ToLower(environment),
		})
	})

	// Listen and serve on 0.0.0.0:8080
	router.Run(":8080")
}
